import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'alcohol'
})
export class AlcoholPipe implements PipeTransform {

  transform(value: any, ...args: unknown[]): unknown {
    const integer = Math.floor(value)
    const decimal = Math.round(value % 1 * 10)
    return integer + ',' + decimal + '%';
  }

}
