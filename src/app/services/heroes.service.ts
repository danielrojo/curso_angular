import { Injectable } from '@angular/core';

@Injectable()
export class HeroesService {

  private heroes = [{ name: 'Batman', description: 'Dark knight' }, { name: 'Spiderman', description: 'Spidy spidy' }];

  constructor() { }

  addHero(hero: any) {
    this.heroes = [...this.heroes, hero];
  }

  getHeroes() {
    return this.heroes;
  }
  
}
