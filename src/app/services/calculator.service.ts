import { Injectable } from '@angular/core';

enum States {
  Init,
  FirstFigure,
  SecondFigure,
  Result
}

@Injectable()
export class CalculatorService {

  private display = '';

  private state = States.Init;
  private firstFigure = 0;
  private secondFigure = 0;
  private result = 0;
  private operator = '';

  constructor() { }

  resolve(): number {
    switch (this.operator) {
      case '+':
        return this.firstFigure + this.secondFigure
      case '-':
        return this.firstFigure - this.secondFigure
      case '*':
        return this.firstFigure * this.secondFigure
      case '/':
        return this.firstFigure / this.secondFigure
      default:
        return -1
    }
  }

  handleNumber(value: number): void {
    switch (this.state) {
      case States.Init:
        this.firstFigure = value;
        this.state = States.FirstFigure;
        this.display += value.toString();
        break;
      case States.FirstFigure:
        this.firstFigure = this.firstFigure * 10 + value;
        this.display += value.toString();
        break;
      case States.SecondFigure:
        this.secondFigure = this.secondFigure * 10 + value;
        this.display += value.toString();
        break;
      case States.Result:
        this.firstFigure = value;
        this.secondFigure = 0;
        this.result = 0;
        this.operator = '';
        this.state = States.FirstFigure;
        this.display = value.toString();
        break;

      default:
        break;
    }
  }

  handleSymbol(symbol: string): void {
    switch (this.state) {
      case States.Init:

        break;
      case States.FirstFigure:
        if (symbol === '+' || symbol === '-' || symbol === '*' || symbol === '/') {
          this.operator = symbol;
          this.state = States.SecondFigure;
          this.display += symbol;
        }
        break;
      case States.SecondFigure:
        if (symbol === '=') {
          this.result = this.resolve();
          this.state = States.Result;
          this.display += symbol + this.result.toString();
        }
        break;
      case States.Result:
        if (symbol === '+' || symbol === '-' || symbol === '*' || symbol === '/') {
          this.firstFigure = this.result;
          this.operator = symbol;
          this.secondFigure = 0;
          this.result = 0;
          this.state = States.SecondFigure;
          this.display = this.firstFigure.toString() + symbol;
        }
        break;

      default:
        break;
    }

  }

  getDisplay(): string {
    return this.display;
  }

}
