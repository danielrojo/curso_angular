import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class ApodService {

  url = 'https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY';

  constructor(public service: HttpClient) { }

  getRequest(date?: any) {
    if(date === undefined) return this.service.get(this.url)
    else {
      const myUrl = this.url + '&date=' + date.year + '-' + date.month + '-' + date.day;
      return this.service.get(myUrl)
    }
  }
}
