import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TrivialService {

  constructor(public service: HttpClient) { }

  getRequest(url = 'https://opentdb.com/api.php?amount=10') {
    return this.service.get(url)
  }
}
