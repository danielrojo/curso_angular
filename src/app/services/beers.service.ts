import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class BeersService {

  constructor(public service: HttpClient) { }

  getRequest(url = 'https://api.punkapi.com/v2/beers') {
    return this.service.get(url)
  }
}
