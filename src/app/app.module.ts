import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { CalculatorComponent } from './components/calculator/calculator.component';
import { DisplayComponent } from './components/display/display.component';
import { KeyboardComponent } from './components/keyboard/keyboard.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeroesComponent } from './components/heroes/heroes.component';
import { FormsModule } from '@angular/forms';
import { HeroeslistComponent } from './components/heroeslist/heroeslist.component';
import { HeroformComponent } from './components/heroform/heroform.component';
import { CalculatorService } from './services/calculator.service';
import { HeroesService } from './services/heroes.service';
import { ApodComponent } from './components/apod/apod.component';
import { HttpClientModule} from '@angular/common/http';
import { ApodService } from './services/apod.service';
import { BeersComponent } from './components/beers/beers.component';
import { BeersService } from './services/beers.service';
import { AlcoholPipe } from './pipes/alcohol.pipe';
import { DateapodComponent } from './components/dateapod/dateapod.component';
import { ShowapodComponent } from './components/showapod/showapod.component';
import { Ng5SliderModule } from 'ng5-slider';
import { FormComponent } from './components/form/form.component';
import { AppRoutingModule } from './app-routing.module';
import { RoutersampleComponent } from './components/routersample/routersample.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { TrivialComponent } from './components/trivial/trivial.component';
import { CardComponent } from './components/card/card.component'; 

@NgModule({
  declarations: [
    AppComponent,
    CalculatorComponent,
    DisplayComponent,
    KeyboardComponent,
    HeroesComponent,
    HeroeslistComponent,
    HeroformComponent,
    ApodComponent,
    BeersComponent,
    AlcoholPipe,
    DateapodComponent,
    ShowapodComponent,
    FormComponent,
    RoutersampleComponent,
    PageNotFoundComponent,
    TrivialComponent,
    CardComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
    Ng5SliderModule,
    AppRoutingModule 
  ],
  providers: [CalculatorService, HeroesService, ApodService, BeersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
