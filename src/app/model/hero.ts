export class Hero {
    // Hero(18, 'Dr IQ', this.powers[0], 'Chuck Overstreet')
    constructor(public id: number, public name: string, public power:string, public alterEgo?:string){}
}