export class Card {

    question = '';
    correctAnswer = '';
    answers: string[] = [];
    responded = false;
    response = '';

    constructor(json?: any) {
        if(json !== undefined) {
            this.question = json.question;
            this.correctAnswer = json.correct_answer;
            this.answers = json.incorrect_answers;
            this.answers = [...this.answers, this.correctAnswer]    
            this.shuffleArray();
        }
    }

    shuffleArray() {
        for (var i = this.answers.length - 1; i > 0; i--) {
            var j = Math.floor(Math.random() * (i + 1));
            var temp = this.answers[i];
            this.answers[i] = this.answers[j];
            this.answers[j] = temp;
        }
    }
}