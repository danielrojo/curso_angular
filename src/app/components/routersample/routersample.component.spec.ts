import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutersampleComponent } from './routersample.component';

describe('RoutersampleComponent', () => {
  let component: RoutersampleComponent;
  let fixture: ComponentFixture<RoutersampleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RoutersampleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutersampleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
