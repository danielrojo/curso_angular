import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';

@Component({
  selector: 'app-routersample',
  templateUrl: './routersample.component.html',
  styleUrls: ['./routersample.component.scss']
})
export class RoutersampleComponent implements OnInit {

  constructor(
      private route: ActivatedRoute,
      private router: Router) { }

  ngOnInit() {
  }

  gotoItems(hero: number) {
      this.router.navigate(['/heroes', { id: hero }]);
  }


}
