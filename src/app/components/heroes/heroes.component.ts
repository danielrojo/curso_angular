import { Component, OnDestroy, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';
import { HeroesService } from 'src/app/services/heroes.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.scss']
})
export class HeroesComponent implements OnInit, OnDestroy {
   
  constructor(public service: HeroesService, public route: Router) { }

  ngOnInit(): void {
    console.log('HeroesComponent OnInit()');    

  }

  ngOnDestroy(): void {
    console.log('HeroesComponent OnDestroy()');    
  }

  getHeroes(){
    return this.service.getHeroes();
  }

  addHero(newHero: any) {
    this.service.addHero(newHero)
  }


}
