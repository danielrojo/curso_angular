import { Component, OnInit } from '@angular/core';
import { ApodService } from 'src/app/services/apod.service';

@Component({
  selector: 'app-apod',
  templateUrl: './apod.component.html',
  styleUrls: ['./apod.component.scss']
})
export class ApodComponent implements OnInit {

  dates: any[] = []

  constructor() { }

  ngOnInit(): void {

  }

  datePicked(value: any) {
    console.log(value);
    this.dates = [...this.dates, value];
  }

}
