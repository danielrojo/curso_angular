import { Component, OnInit } from '@angular/core';
import { Card } from 'src/app/model/card';
import { TrivialService } from 'src/app/services/trivial.service';

@Component({
  selector: 'app-trivial',
  templateUrl: './trivial.component.html',
  styleUrls: ['./trivial.component.scss']
})
export class TrivialComponent implements OnInit {

  score = 0;
  public cards: Card[] = []

  constructor(public service: TrivialService) { }

  ngOnInit(): void {
    this.service.getRequest().subscribe(
      (data) => this.processData(data),
      (error) => this.processError(error)
    )
  }

  processData(data: any) {
    console.log(data);
    data.results.forEach((element: any) => {
      this.cards = [...this.cards, new Card(element)]
    });

  }

  processError(error: any) {

  }

  handleResponse(response: boolean) {
    if (response) this.score += 2;
    else this.score -= 1;
  }

}
