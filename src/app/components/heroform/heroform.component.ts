import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-heroform',
  templateUrl: './heroform.component.html',
  styleUrls: ['./heroform.component.scss']
})
export class HeroformComponent implements OnInit {

  @Output() onNewHero = new EventEmitter<any>();
  newHeroName = '';
  newHeroDescription = '';

  constructor() { }

  ngOnInit(): void {
  }

  getClass(): string {
    if(this.newHeroName === ''){
      return 'btn btn-secondary mt-2';
    }
    return 'btn btn-primary mt-2';
  }

  isNameEmpty(): boolean {
    return (this.newHeroName === '')
  }

  addHero() {
    this.onNewHero.emit({ name: this.newHeroName, description: this.newHeroDescription });
    this.newHeroName = '';
    this.newHeroDescription = '';
  }

}
