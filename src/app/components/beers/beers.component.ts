import { Component, OnInit } from '@angular/core';
import { Options } from 'ng5-slider';
import { BeersService } from 'src/app/services/beers.service';

@Component({
  selector: 'app-beers',
  templateUrl: './beers.component.html',
  styleUrls: ['./beers.component.scss']
})
export class BeersComponent implements OnInit {

  beers: any[] = [];
  showBeers: any[] = [];
  minValue: number = 3;
  maxValue: number = 5;
  showSlider = true
  options: Options = {
    floor: 0,
    ceil: 60,
    step: 0.1,
    disabled: false
  };

  constructor(public service: BeersService) { }

  ngOnInit(): void {
    this.service.getRequest().subscribe(
      (data) => this.processData(data),
      (error) => this.processError(error)
    )
  }

  handleChange() {
    console.log(this.minValue.toString() + ':' + this.maxValue.toString());
    this.showBeers = this.beers
      .filter(beer => (beer.abv >= this.minValue) && (beer.abv <= this.maxValue))
      .sort((a, b) => a.abv - b.abv)
  }

  handleClick() {
    console.log('entra');
    this.showSlider = !this.showSlider;
    if (this.showSlider) this.showBeers = this.beers
    .filter(beer => (beer.abv >= this.minValue) && (beer.abv <= this.maxValue))
    .sort((a, b) => a.abv - b.abv)
    else this.showBeers = this.beers;

  }

  processData(data: any) {
    console.log(data);
    this.beers = data;
    this.showBeers = this.beers
    .filter(beer => (beer.abv >= this.minValue) && (beer.abv <= this.maxValue))
    .sort((a, b) => a.abv - b.abv);

  }

  processError(error: any) {

  }

}
