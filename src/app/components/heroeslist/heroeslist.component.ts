import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-heroeslist',
  templateUrl: './heroeslist.component.html',
  styleUrls: ['./heroeslist.component.scss']
})
export class HeroeslistComponent implements OnInit {

  @Input() myHeroes: any[] = []
  constructor() { }

  ngOnInit(): void {
  }

}
