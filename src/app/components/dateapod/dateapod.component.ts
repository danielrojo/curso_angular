import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-dateapod',
  templateUrl: './dateapod.component.html',
  styleUrls: ['./dateapod.component.scss']
})
export class DateapodComponent implements OnInit {

  @Output() onDatePicked = new EventEmitter<any>();
  model: NgbDateStruct | undefined;
  
  constructor() { }

  ngOnInit(): void {
  }

  datePicked(value: any) {
    console.log(value);
    this.onDatePicked.emit(value)    

  }

}
