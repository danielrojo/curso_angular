import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DateapodComponent } from './dateapod.component';

describe('DateapodComponent', () => {
  let component: DateapodComponent;
  let fixture: ComponentFixture<DateapodComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DateapodComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DateapodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
