import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Card } from 'src/app/model/card';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {

  @Input() info: Card = new Card();
  @Output() onResponded = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
  }

  handleClick(answer: string) {
    this.info.responded = true;
    this.info.response = answer;
    if(answer === this.info.correctAnswer) this.onResponded.emit(true)
    else this.onResponded.emit(false)
  }

  getClass(answer: string): string {
    if(this.info.responded === false) {
      return 'btn btn-primary btn-block'
    }else {
      if(answer === this.info.correctAnswer) return 'btn btn-success btn-block'
      else if(this.info.response === answer) return 'btn btn-danger btn-block'
      else return 'btn btn-secondary btn-block'
    }
  }

}
