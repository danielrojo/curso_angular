import { Component, OnInit } from '@angular/core';
import { CalculatorService } from 'src/app/services/calculator.service';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent implements OnInit {

  display = '';

  constructor(public service: CalculatorService) { }
  
  handleClick(value: any) {
    if (typeof value === 'number') {
      this.service.handleNumber(value)
    } else if (typeof value === 'string') {
      this.service.handleSymbol(value)
    }
    this.display = this.service.getDisplay();
  }

  ngOnInit(): void {
    this.display = this.service.getDisplay();
  }

}
