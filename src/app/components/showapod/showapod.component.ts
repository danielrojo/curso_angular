import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ApodService } from 'src/app/services/apod.service';

@Component({
  selector: 'app-showapod',
  templateUrl: './showapod.component.html',
  styleUrls: ['./showapod.component.scss']
})
export class ShowapodComponent implements OnInit, OnChanges {

  @Input() date = {}
  data: any = {}
  constructor(public service: ApodService) { }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('ngOnchanges()');
    
    if (this.date !== undefined)
      this.service.getRequest(this.date).subscribe(
        (data) => this.processData(data),
        (error) => this.processError(error)
      )
  }

  ngOnInit(): void {

  }


  processData(data: any) {
    console.log(data);
    this.data = data;

  }

  processError(error: any) {

  }

}
